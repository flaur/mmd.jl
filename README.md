# MMD.jl

[![Build Status](https://gitlab.com/flaur/mmd.jl/badges/main/pipeline.svg)](https://gitlab.com/flaur/mmd.jl/pipelines)
[![Coverage](https://gitlab.com/flaur/mmd.jl/badges/main/coverage.svg)](https://gitlab.com/flaur/mmd.jl/commits/main)
[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/flaur/mmd.jl/-/blob/main/LICENSE)

This implementation focuses on the Gaussian kernel and its application to testing differences in mean between a large control group and many smaller alternative groups.

The kernel width is typically adjusted to the control group, and intermediate calculations for the control kernel are stored and reused as much as possible.
