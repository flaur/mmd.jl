using MMD
using Test
using StatsBase, Random, StableRNGs, HypothesisTests
import IPMeasures

include("naive.jl")

@testset "vs IPMeasures" begin

    # in the case IPMeasures.mmd ever changes, here follows the values returned
    ipmeasures_mmds = [
    -0.0017514623802886709,
    0.007382865681137218,
    0.006984208087042032,
    0.004551474796168908,
    -0.004795706749622153,
    -0.0021592622489965407,
    0.011071256813369956,
    -0.003439893390193016,
    0.002366577401847203,
    0.004501037199519442,
    ]

    rng = StableRNG(91047820)
    for _ in 1:10
        seed = Random.rand(rng, UInt)
        rng′ = StableRNG(seed)

        X = Random.randn(rng′, 100, 2)
        Y = Random.randn(rng′, 100, 2)
        γ = 0.3
        kx, ky, kxy = makekernels(X, Y, γ)

        Random.seed!(rng′, seed)
        I, I′, J, J′ = MMD.surrogate_split(rng′, size(kxy.K))
        X′ = [X[I,:]; Y[J,:]]
        Y′ = [X[I′,:]; Y[J′,:]]
        mmd′ = mmd(X′, Y′, γ)

        Random.seed!(rng′, seed)
        mmd″ = MMD.surrogate_mmd(rng′, kx, ky, kxy)
        #info abs(mmd′ - mmd″) mmd′ mmd″
        @test abs(mmd′ - mmd″) < 1e-14

        mmd‴ = IPMeasures.mmd(IPMeasures.GaussianKernel(γ), collect(transpose(X′)), collect(transpose(Y′)))
        #@info mmd‴ abs(mmd′ - mmd‴) abs(mmd″ - mmd‴)
        @test abs(mmd′ - mmd‴) < 1e-14
        @test abs(mmd″ - mmd‴) < 1e-14
    end
end

# duplicate code from surrogate_mmd2u for testing purposes only
function test_surrogate_mmd2u(rng::AbstractRNG, x, y, kx, ky, kxy, args...)
    surrogate_split = MMD.surrogate_split
    OneSampleKernel = MMD.OneSampleKernel
    TwoSampleKernel = MMD.TwoSampleKernel
    kernelsum = MMD.kernelsum
    kernelmean = MMD.kernelmean
    mmd2u = MMD.mmd2u

    @assert kx.γ == ky.γ == kxy.γ
    I′, I″, J′, J″ = surrogate_split(rng, size(kxy.K), args...)

    ## kx′:
    # the full kernel matrix would be as follows:
    #Kx = [kx.K[I′,I′] kxy.K[I′,J′]; transpose(kxy.K[I′,J′]) ky.K[J′,J′]]
    # instead, we first compute the kernel sums consuming the matrix elements as they are accessed, with:
    # 1. sum of kx.K[I′,I′] (diagonal excluded)
    kx_sum = kernelsum(kx.K, I′)
    # 2. sum of kxy.K[I′,J′] and transpose(kxy.K[I′,J′])
    kx_sum += 2kernelsum(kxy.K, I′, J′)
    # 3. sum of ky.K[J′,J′] (diagonal excluded)
    kx_sum += kernelsum(ky.K, J′)
    # at last, we derive the kernel mean
    kx_n = length(I′) + length(J′)
    kx_mean = kx_sum / kx_n / (kx_n - 1)

    x′ = [x[I′,:]; y[J′,:]]
    kx′ = OneSampleKernel(x′, kx.γ)
    @test abs(kx_mean - kernelmean(kx′)) < 1e-14

    ## ky′:
    #Ky = [kx.K[I″,I″] kxy.K[I″,J″]; transpose(kxy.K[I″,J″]) ky.K[J″,J″]]
    ky_sum = kernelsum(kx.K, I″)
    ky_sum += 2kernelsum(kxy.K, I″, J″)
    ky_sum += kernelsum(ky.K, J″)
    ky_n = length(I″) + length(J″)
    ky_mean = ky_sum / ky_n / (ky_n - 1)

    y′ = [x[I″,:]; y[J″,:]]
    ky′ = OneSampleKernel(y′, kx′.γ)
    @test abs(ky_mean - kernelmean(ky′)) < 1e-14

    ## kxy′:
    #Kxy = [kx.K[I′,I″] kxy.K[I′,J″]; transpose(kxy.K[I″,J′]) ky.K[J′,J″]]
    kxy_sum = kernelsum(kx.K, I′, I″)
    kxy_sum += kernelsum(kxy.K, I′, J″)
    kxy_sum += kernelsum(kxy.K, I″, J′)
    kxy_sum += kernelsum(ky.K, J′, J″)
    kxy_mean = kxy_sum / kx_n / ky_n

    kxy′ = TwoSampleKernel(x′, y′, kx′, ky′)
    @test abs(kxy_mean - kernelmean(kxy′)) < 1e-14

    mmd2u′ = kx_mean + ky_mean - 2kxy_mean
    @test abs(mmd2u′ - mmd2u(kx′, ky′, kxy′)) < 1e-14
    @test abs(mmd2u′ - mmd2u(kx′, ky′, kxy′, 1:size(x′,1))) < 1e-14
end

@testset "Kernel resampling" begin
    α = .05
    ntrials = 50
    pdf_integral = MMD.binomialcdf(ntrials, ntrials, α)
    @test abs(pdf_integral - 1) < 1e-14

    # the following tests are redundant with the previous test set;
    # they were included to narrow the possibilities down, in the search
    # for unexplained false alarms, and have been left since they take
    # negligible time to run

    rng = StableRNG(91047820)
    m, n = 100, 50
    x = Random.randn(rng, m, 2)
    y = Random.randn(rng, n, 2)
    I = sort(Random.randperm(m)[1:n])
    kx = referencekernel(x)
    kx, ky, kxy = makekernels(x, y, kx)
    test_surrogate_mmd2u(rng, x, y, kx, ky, kxy, I)

    x′ = x[I,:]
    kx′, ky′, kxy′ = makekernels(x′, y, kx.γ)
    mmd′ = mmd(kx′, ky′, kxy′)
    mmd″ = mmd(kx, ky, kxy, I)
    @test abs(mmd′ - mmd″) < 1e-14

    pvalue = mmd_test(copy(rng), kx, ky, kxy)
    @test pvalue == mmd_test(rng, kx, ky, kxy, 1:size(x,1), 1:size(y,1))

    pvalue′ = mmd_test(copy(rng), kx′, ky′, kxy′)
    pvalue″ = mmd_test(rng, kx, ky, kxy, I)
    @test abs(pvalue′ - pvalue″) < 1e-14
end

@testset "Type-1 error rate" begin
    α = .05
    ntrials = 50
    m, n = 200, 100

    rng = StableRNG(91047820)

    mmds = Float64[]
    false_alarms = 0
    resampling_false_alarms = 0
    bootstrap_false_alarms = 0
    for _ in 1:ntrials
        x = Random.randn(rng, m, 5)
        y = Random.randn(rng, n, 5)
        I = sort(Random.randperm(rng, m)[1:n])

        mmdvalue = mmd(x, y, .3)
        push!(mmds, mmdvalue)

        pvalue = mmd_test(rng, x, y; γ=.3)
        α < pvalue || (false_alarms += 1)

        kx = referencekernel(x)
        _, ky, kxy = makekernels(x, y, kx)
        resampling_pvalue = mmd_test(rng, kx, ky, kxy, I)
        α < resampling_pvalue || (resampling_false_alarms += 1)

        bootstrap_pvalue = bootstrap_mmd_test(rng, x, y)
        α < bootstrap_pvalue || (bootstrap_false_alarms += 1)
    end

    #@info maximum(mmds)
    @test all(<=(0.011), mmds)

    0 < false_alarms < α * ntrials && @info "False alarms" false_alarms
    @test false_alarms < α * ntrials

    0 < resampling_false_alarms < α * ntrials && @info "False alarms (resampling)" resampling_false_alarms
    @test resampling_false_alarms < α * ntrials

    0 < bootstrap_false_alarms < α * ntrials && @info "False alarms (bootstrap)" bootstrap_false_alarms
    @test bootstrap_false_alarms < α * ntrials
end
