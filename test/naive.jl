
function buggy_surrogate_split(rng, m::Integer, n::Integer; min_sample_size=0)
    p, q = min_sample_size, min_sample_size - 2
    while true
        ij = Random.randperm(rng, m + n)
        I, J = sort(ij[1:m]), sort(ij[m+1:m+n])
        i = searchsortedfirst(I, m + 1)
        p < i < m - q || continue
        I′, J′ = I[1:i-1], I[i:m] .- m
        j = searchsortedfirst(J, m + 1)
        p < j < n - q || continue
        I″, J″ = J[1:j-1], J[j:n] .- m
        #@assert sort([I′;I″]) == 1:m
        #@assert sort([J′;J″]) == 1:n
        return I′, I″, J′, J″
    end
end

function naive_mmd_test(rng::AbstractRNG, x, y, γ::Real; surrogates=100)
    m, n = size(x, 1), size(y, 1)
    challenger_mmd = mmd(makekernels′(x, y, γ)...)

    surrogate_mmds = Float64[]
    sizehint!(surrogate_mmds, surrogates)
    for _ in 1:surrogates
        I′, I″, J′, J″ = surrogate_split′(rng, (m, n))
        x′ = [x[I′,:]; y[J′,:]]
        y′ = [x[I″,:]; y[J″,:]]
        mmd′ = mmd(makekernels′(x′, y′, γ)...)
        push!(surrogate_mmds, mmd′)
    end

    failures = count(>=(challenger_mmd), surrogate_mmds)
    pvalue = (failures + 1) / (surrogates + 1)
end
function naive_mmd_test(rng::AbstractRNG, x, y,
    I::Union{Nothing,AbstractVector{Int}}=nothing,
    J::Union{Nothing,AbstractVector{Int}}=nothing;
    γ=nothing, kwargs...)
    if isnothing(γ)
        γ = referencekernel(x).γ
    end
    if !isnothing(I)
        x = x[I,:]
        if !isnothing(J)
            y = y[J,:]
        end
    end
    naive_mmd_test(rng, x, y, γ; kwargs...)
end
function naive_mmd_test(args...; kwargs...)
    naive_mmd_test(Random.default_rng(), args...; kwargs...)
end

function makekernels′(x, y, γ)
    kx = OneSampleKernel(x, γ)
    ky = OneSampleKernel(y, γ)
    kxy = TwoSampleKernel(x, y; γ=γ)
    return kx, ky, kxy
end
