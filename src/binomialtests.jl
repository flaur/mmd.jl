
function logbinomial(n, k)
    k ∈ (0, n) && return 0.0 # log(1.0)
    log1n = log.(1:n)
    sum(log1n[(n - k + 1):n]) - sum(log1n[1:k])
end
function binomialpdf(n, k, α)
    exp(logbinomial(n, k) + log(α) * k + log(1-α) * (n - k))
end
binomialcdf(n, k, α) = sum(binomialpdf(n, i, α) for i in 0:k)
function onesided_binomial_test(pvalues::AbstractVector, α)
    total = length(pvalues)
    significant = count(<=(α), pvalues)
    onesided_binomial_test(total, significant, α)
end
function onesided_binomial_test(n, k, α)
    # condition for a one-sided test:
    # the observed k must be larger than the expected number of successes
    α * n < k || return 1.0
    pvalue = 1 - binomialcdf(n, k - 1, α)
    # or, equivalently:
    #pvalue = sum(binomialpdf(n, i, α) for i in k:n)
    if !(0 <= pvalue <= 1)
        @warn "Assertion failed: 0 <= pvalue <= 1" pvalue
        if -1e-6 <= pvalue <= 1+1e-6
            pvalue = max(0, min(1, pvalue))
        else
            throw("Assertion failed: 0 <= pvalue <= 1")
        end
    end
    return pvalue
end

