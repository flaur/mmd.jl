
function surrogate_split(rng, m::Integer, n::Integer; min_sample_size=1)
    @assert n <= m
    k = Random.rand(rng, 1:n-min_sample_size)
    i = m - n + k
    j = n - k
    #
    I = Random.randperm(rng, m)
    J = Random.randperm(rng, n)
    I′, I″ = sort(I[1:i]), sort(I[i+1:end])
    J′, J″ = sort(J[1:j]), sort(J[j+1:end])
    return I′, I″, J′, J″
end
function surrogate_split(rng, siz::Tuple{Int, Int}; kwargs...)
    surrogate_split(rng, siz...; kwargs...)
end
function surrogate_split(rng, siz::Tuple{Int, Int}, I; kwargs...)
    m = length(I)
    n = siz[2]
    I′, I″, J′, J″ = surrogate_split(rng, m, n; kwargs...)
    return I[I′], I[I″], J′, J″
end
function surrogate_split(rng, ::Tuple{Int, Int}, I, J; kwargs...)
    m = length(I)
    n = length(J)
    I′, I″, J′, J″ = surrogate_split(rng, m, n; kwargs...)
    return I[I′], I[I″], J[J′], J[J″]
end

function mmd_test(rng::AbstractRNG, kx, ky, kxy, args...; surrogates=100)
    challenger_mmd = mmd(kx, ky, kxy, args...)
    surrogate_mmds = [surrogate_mmd(rng, kx, ky, kxy, args...) for _ in 1:surrogates]
    failures = count(>=(challenger_mmd), surrogate_mmds)
    pvalue = (failures + 1) / (surrogates + 1)
end
function mmd_test(rng::AbstractRNG, x::Matrix{T}, y::Matrix{T}, args...;
    γ=nothing, kwargs...) where {T<:Real}
    kx, ky, kxy = isnothing(γ) ? makekernels(x, y) : makekernels(x, y, γ)
    mmd_test(rng, kx, ky, kxy, args...; kwargs...)
end
mmd_test(args...; kwargs...) = mmd_test(Random.default_rng(), args...; kwargs...)

function bootstrap_mmd_test(rng::AbstractRNG, x, y;
    bootstrap=nothing, α=0.05, kx=nothing, kwargs...)
    m = size(x, 1)
    n = size(y, 1)
    m >= 2n || throw("x is not large enough compared to y, for bootstrap")

    kref = isnothing(kx) ? referencekernel(x) : kx
    kx, ky, kxy = makekernels(x, y, kref)

    if isnothing(bootstrap)
        bootstrap = round(Int, 5m / n)
    end

    bootstrap_pvalues = Float64[]
    sizehint!(bootstrap_pvalues, bootstrap)
    for _ in 1:bootstrap
        I = sort(Random.randperm(rng, m)[1:n])
        pvalue = mmd_test(rng, kx, ky, kxy, I; kwargs...)
        push!(bootstrap_pvalues, pvalue)
    end

    #test = BinomialTest(bootstrap_pvalues .<= α, α)
    #pvalue = HypothesisTests.pvalue(test; tail=:right)
    #@assert 0 <= pvalue <= 1

    alt_pvalue = onesided_binomial_test(bootstrap_pvalues, α)
    return alt_pvalue
end
function bootstrap_mmd_test(args...; kwargs...)
    bootstrap_mmd_test(Random.default_rng(), args...; kwargs...)
end

