
struct OneSampleKernel{T}
    γ::T
    x2::Union{Nothing,Matrix{T}}
    K::Matrix{T}
end

struct TwoSampleKernel{T}
    γ::T
    K::Matrix{T}
end

function OneSampleKernel(x::Matrix{T}, γ::Union{Nothing,T}=nothing, x2=nothing) where {T<:Real}
    if isnothing(x2)
        x2 = sum(x .* x; dims=2)
    end
    D2 = (-2x * transpose(x)) .+ x2 .+ transpose(x2)
    if isnothing(γ)
        n = size(x, 1)
        medianD2 = median(D2[i,j] for i in 1:n-1 for j in i+1:n)
        γ = 1 / 2medianD2
    end
    K = @. exp(-γ * D2)
    OneSampleKernel(γ, x2, K)
end
function OneSampleKernel(γ::T, K::Matrix{T}) where {T<:Real}
    OneSampleKernel(γ, nothing, K)
end

function TwoSampleKernel(x::Matrix{T}, y::Matrix{T}; kwargs...) where {T<:Real}
    x2 = sum(x .* x; dims=2)
    y2 = sum(y .* y; dims=2)
    TwoSampleKernel(x, y, x2, y2; kwargs...)
end
function TwoSampleKernel(x::Matrix{T}, y::Matrix{T}, x2::Matrix{T}, y2::Matrix{T}; kwargs...) where {T<:Real}
    D2 = (x * transpose(-2y)) .+ x2 .+ transpose(y2)
    TwoSampleKernel(D2; kwargs...)
end
function TwoSampleKernel(D2::Matrix{T}; γ::Union{Nothing,T}=nothing) where {T<:Real}
    if isnothing(γ)
        γ = 1 / 2median(D2)
    end
    K = @. exp(-γ * D2)
    TwoSampleKernel(γ, K)
end
function TwoSampleKernel(x::Matrix{T}, y::Matrix{T}, kx::OneSampleKernel{T}, ky::OneSampleKernel{T}) where {T<:Real}
    @assert kx.γ == ky.γ
    γ = kx.γ
    x2 = kx.x2
    y2 = ky.x2
    TwoSampleKernel(x, y, x2, y2; γ=γ)
end

function kernelmean(kernel::OneSampleKernel)
    K = kernel.K
    n = size(K, 1)
    mean(K[i,j] for i in 1:n-1 for j in i+1:n)
end
function kernelmean(kernel::OneSampleKernel, I)
    K = kernel.K
    n = length(I)
    @assert issorted(I)
    mean(K[I[i],I[j]] for i in 1:n-1 for j in i+1:n)
end
kernelmean(kernel::TwoSampleKernel) = mean(kernel.K)
function kernelmean(kernel::TwoSampleKernel, I)
    K = kernel.K
    m = length(I)
    n = size(K, 2)
    mean(K[I[i],j] for i in 1:m for j in 1:n)
end
function kernelmean(kernel::TwoSampleKernel, I, J)
    K = kernel.K
    m = length(I)
    n = length(J)
    mean(K[I[i],J[j]] for i in 1:m for j in 1:n)
end

referencekernel(x) = OneSampleKernel(x)

function makekernels(x, y)
    x2 = sum(x .* x; dims=2)
    y2 = sum(y .* y; dims=2)
    kxy = TwoSampleKernel(x, y, x2, y2)
    γ = kxy.γ
    kx = OneSampleKernel(x, γ, x2)
    ky = OneSampleKernel(y, γ, y2)
    #m, n = size(x,1), size(y,1)
    #@assert size(kx.K) == (m,m)
    #@assert size(ky.K) == (n,n)
    #@assert size(kxy.K) == (m,n)
    return kx, ky, kxy
end
function makekernels(x, y, kx::OneSampleKernel)
    ky = OneSampleKernel(y, kx.γ)
    kxy = TwoSampleKernel(x, y, kx, ky)
    return kx, ky, kxy
end
makekernels(x, y, γ) = makekernels(x, y, OneSampleKernel(x, γ))

function kernelsum(K, I)
    # sum of K[I,I] minus its trace,
    # or equivalently twice the sum of triu(K[I,I], 1)
    n = length(I)
    n < 2 && return 0
    2sum(K[I[i],I[j]] for i in 1:n-1 for j in i+1:n)
end
# sum of K[I,J] == sum of K[J,I]
kernelsum(K, I, J) = sum(K[I,J])

