module MMD

using StatsBase, Random

export referencekernel, makekernels, mmd, mmd_test, bootstrap_mmd_test

include("kernels.jl")
include("mmds.jl")
include("mmdtests.jl")
include("binomialtests.jl")

end
