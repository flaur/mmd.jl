
function mmd2u(kx, ky, kxy, args...)
    @assert kx.γ == ky.γ == kxy.γ
    mmd2u′(kx, ky, kxy, args...)
end
function mmd2u′(kx, ky, kxy)
    kernelmean(kx) + kernelmean(ky) - 2kernelmean(kxy)
end
function mmd2u′(kx, ky, kxy, I)
    kernelmean(kx, I) + kernelmean(ky) - 2kernelmean(kxy, I)
end
function mmd2u′(kx, ky, kxy, I, J)
    kernelmean(kx, I) + kernelmean(ky, J) - 2kernelmean(kxy, I, J)
end
function mmd2u(x::Matrix{T}, y::Matrix{T}, args...) where {T<:Real}
    mmd2u(makekernels(x, y, args...)...)
end

const mmd = mmd2u

function surrogate_mmd2u(rng::AbstractRNG, kx, ky, kxy, args...; kwargs...)
    @assert kx.γ == ky.γ == kxy.γ
    I′, I″, J′, J″ = surrogate_split(rng, size(kxy.K), args...; kwargs...)

    ## kx′:
    # the full kernel matrix would be as follows:
    #Kx = [kx.K[I′,I′] kxy.K[I′,J′]; transpose(kxy.K[I′,J′]) ky.K[J′,J′]]
    # instead, we first compute the kernel sums consuming the matrix elements as they are accessed, with:
    # 1. sum of kx.K[I′,I′] (diagonal excluded)
    kx_sum = kernelsum(kx.K, I′)
    # 2. sum of kxy.K[I′,J′] and transpose(kxy.K[I′,J′])
    kx_sum += 2kernelsum(kxy.K, I′, J′)
    # 3. sum of ky.K[J′,J′] (diagonal excluded)
    kx_sum += kernelsum(ky.K, J′)
    # at last, we derive the kernel mean
    kx_n = length(I′) + length(J′)
    kx_mean = kx_sum / kx_n / (kx_n - 1)

    ## ky′:
    #Ky = [kx.K[I″,I″] kxy.K[I″,J″]; transpose(kxy.K[I″,J″]) ky.K[J″,J″]]
    ky_sum = kernelsum(kx.K, I″)
    ky_sum += 2kernelsum(kxy.K, I″, J″)
    ky_sum += kernelsum(ky.K, J″)
    ky_n = length(I″) + length(J″)
    ky_mean = ky_sum / ky_n / (ky_n - 1)

    ## kxy′:
    #Kxy = [kx.K[I′,I″] kxy.K[I′,J″]; transpose(kxy.K[I″,J′]) ky.K[J′,J″]]
    kxy_sum = kernelsum(kx.K, I′, I″)
    kxy_sum += kernelsum(kxy.K, I′, J″)
    kxy_sum += kernelsum(kxy.K, I″, J′)
    kxy_sum += kernelsum(ky.K, J′, J″)
    kxy_mean = kxy_sum / kx_n / ky_n

    return kx_mean + ky_mean - 2kxy_mean
end
function surrogate_mmd2u(args...; kwargs...)
    surrogate_mmd2u(Random.default_rng(), args...; kwargs...)
end

const surrogate_mmd = surrogate_mmd2u

